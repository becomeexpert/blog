import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { Post } from '../entity/post';


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {
  posts: Post[];
  postSubscription : Subscription;
  constructor(
    private postService: PostsService) { }

  ngOnInit() {
    this.chargerListPosts();
  } 
  // chargement des posts
  chargerListPosts(){
    this.postSubscription = this.postService.postSubject.subscribe(
      (posts:Post[])=>{
        this.posts = posts;
      }
    );
    this.postService.getPosts();
  }
 
 
  ngOnDestroy(){
    this.postSubscription.unsubscribe();
  }
}
