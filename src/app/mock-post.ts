import { Post } from "./entity/post";

export const POSTS: Post[] = [
    {
      "title": "LIMOZEN",
      "content": "Mollit sint aliqua eu aliquip dolor dolor dolor velit veniam exercitation officia nostrud anim ea. Adipisicing nostrud voluptate sunt qui do aliqua Lorem occaecat quis ipsum cupidatat magna ea. Do eiusmod aliqua cupidatat reprehenderit sit reprehenderit non incididunt ullamco mollit dolore proident aliquip. Reprehenderit Lorem cillum officia id occaecat. Duis voluptate qui velit anim consectetur. Et voluptate ut in exercitation. Minim pariatur minim qui sunt.\r\n",
      "loveIts": 0,
      "created_at": "2018-08-29T11:01:20 -01:00"
    },
    {
      "title": "SIGNIDYNE",
      "content": "Aute laboris consectetur sit veniam id laboris anim officia eiusmod fugiat anim commodo officia deserunt. Pariatur duis duis tempor consequat aliquip anim consequat. In est incididunt exercitation id proident magna pariatur aliqua in esse eu ipsum sit adipisicing. Sint ea incididunt et deserunt et fugiat sit aute id. Velit reprehenderit nisi labore anim ut officia quis pariatur cupidatat deserunt ex pariatur.\r\n",
      "loveIts": 0,
      "created_at": "2014-10-10T01:27:07 -01:00"
    },
    {
      "title": "PLAYCE",
      "content": "Laboris elit ipsum minim dolor consectetur commodo elit ipsum laboris dolore ex. Duis id ullamco qui pariatur nostrud. Pariatur et qui dolore nisi elit aute.\r\n",
      "loveIts": 0,
      "created_at": "2014-02-05T05:05:08 -01:00"
    },
    {
      "title": "MITROC",
      "content": "Duis incididunt sint exercitation laboris minim magna occaecat. Ullamco consequat laborum ex ea eiusmod incididunt reprehenderit proident ex sint ut cillum. Consectetur sunt aliqua ipsum minim.\r\n",
      "loveIts": 0,
      "created_at": "2014-07-07T11:10:20 -01:00"
    },
    {
      "title": "QUILCH",
      "content": "Deserunt dolore voluptate culpa aute dolore dolore nostrud tempor ea proident excepteur cupidatat. Veniam qui consequat magna anim enim proident proident. Eiusmod in aute qui tempor Lorem consectetur excepteur do cupidatat esse ullamco. Pariatur eu velit mollit magna mollit aute exercitation ipsum non aliquip officia qui aliquip aute.\r\n",
      "loveIts": 0,
      "created_at": "2018-05-22T11:40:43 -01:00"
    },
    {
      "title": "MIRACLIS",
      "content": "Labore ullamco ex commodo id ad officia aute excepteur voluptate qui incididunt aliquip sint deserunt. Ex anim ullamco veniam veniam magna ipsum excepteur nostrud proident dolor deserunt ut officia magna. Eu laborum commodo veniam nulla aliqua laborum occaecat ea ex sit sit esse. Ex duis Lorem consequat aliquip tempor sit ut Lorem et. Sunt excepteur pariatur officia incididunt ut Lorem proident sunt duis adipisicing proident non nisi qui. Cillum ipsum aliquip Lorem Lorem labore nulla occaecat et veniam consectetur est quis tempor. Incididunt veniam nostrud officia laborum reprehenderit magna.\r\n",
      "loveIts": 0,
      "created_at": "2017-07-20T06:58:11 -01:00"
    }
  ];
