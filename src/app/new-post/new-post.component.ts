import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { Post } from '../entity/post';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {


  postsForm : FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private postService: PostsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initPostForm();
  }
  initPostForm(){
    this.postsForm =this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }
  onSavePost(){
    const title = this.postsForm.get('title').value;
    const content = this.postsForm.get('content').value;
    const newpost = new Post(title,content);
    newpost.created_at = new Date();
    newpost.loveIts =0;
    this.postService.createPost(newpost);
    this.router.navigate(['/posts']);
  }

}
