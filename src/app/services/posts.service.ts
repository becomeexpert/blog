import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import { Post } from '../entity/post';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  posts: Post[] = [];
  postSubject = new Subject<Post[]>();
  constructor() { }
// emission des posts
  emettrePosts(){
    this.postSubject.next(this.posts);
  }
  // enregistrement de la liste des posts
  postPost(){
    firebase.database().ref('/posts').set(this.posts);
  }
  //création d'un nouveau post
  createPost(post:Post){
    this.posts.push(post);
    this.postPost();
    this.emettrePosts();
  }
  //recupérationliste des posts
  getPosts(){
    firebase.database().ref('/posts').on('value', (data)=>{
        this.posts = data.val()? data.val():[];
        this.emettrePosts();
    });
  }
  // supprimer un post
  detelePost(post:Post){
    const postIndexToDelete =this.posts.findIndex(
      (postEl) =>{
        if (postEl===post) {
          return true;
        }
      }
    );
    this.posts.splice(postIndexToDelete,1);
    this.postPost();
    this.emettrePosts();
  }
  getSinglePost(id:number){
    console.log("before promise: "+id);
    return new Promise(
      (resolve,reject) =>{
        firebase.database().ref('/posts/' + id).once('value').then(
          (data) =>{
            console.log(data);
            resolve(data.val());
          }, (error) =>{
            reject(error);
          }
        );
      });
  }
  updatePost(post:Post,index:number){
    
    firebase.database().ref('/posts/'+index).update(post);
    this.getPosts();
    this.emettrePosts();
  }
}
