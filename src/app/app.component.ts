import { Component } from '@angular/core';
import { POSTS } from './mock-post';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';
   constructor(){
      // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAgrdPfGs_gqkp6MTFFM5b9nDGeto3GOUY",
    authDomain: "blog-36199.firebaseapp.com",
    databaseURL: "https://blog-36199.firebaseio.com",
    projectId: "blog-36199",
    storageBucket: "",
    messagingSenderId: "596720485920"
  };
  firebase.initializeApp(config);
   }

}
