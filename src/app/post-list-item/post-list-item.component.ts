import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../services/posts.service';
import { Post } from '../entity/post';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() post;
  @Input() index;
 
 
  constructor(private postService:PostsService) {
   
   }

  ngOnInit() {
    console.log("Index: "+this.index);
  }
 
 /**
   * Appel lors du clic sur le 
   * button Love It
   */
  onLikeIt(){
    this.post.loveIts +=1;
    this.postService.updatePost(this.post,this.index);
    //this.postService.createPost(this.post);
    console.log("like: "+this.post.loveIts);
  }
  /**
   * Appel lor du clic sur le 
   * button Don't love it
   */
  onDelikeIt(){
    this.post.loveIts -=1;
    console.log("Index: "+this.index);
    this.postService.updatePost(this.post,this.index);
   
  }
  getLoveIt(){
    if(this.post.loveIts>0){
      return this.post.loveIts;
    }
    return 0;
  }
  getDeLoveIt(){
    if(this.post.loveIts<0){
      return (-1)*this.post.loveIts;
    }
    return 0;
  }
  onDeletePost(post:Post){
    this.postService.detelePost(post);
  }
  

}
